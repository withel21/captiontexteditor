﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaptionTextEditor
{
    public class Page
    {
        public TextAlign align = TextAlign.Unknown;
        public List<string> textLines = null;
        private int page = -1;
       
        public Page( List<String> pageString, int pageNo) {
            page = pageNo;
            textLines = new List<string>();

            align = Document.GetAlignFromLineText(pageString[0]);
            foreach( string aLine in pageString)
            {
                if( Document.IsStartFromAlignId( aLine))
                {
                    textLines.Add(aLine.Substring(1));
                }
                else
                {
                    textLines.Add(aLine);
                }
            }
        }

        public int GetPageNo() { return page; }

        public List<string> GetPageForDocument()
        {
            List<string> pageString = new List<string>();
            string prefix = Document.GetAlignPrefix(align);

            foreach( string aLine in textLines)
            {
                pageString.Add(prefix + aLine);
            }
            return pageString;
        }
    }
}
