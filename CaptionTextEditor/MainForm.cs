﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaptionTextEditor
{
    public enum TextAlign { Left, Center, Right, Unknown };

    public partial class MainForm : Form
    {

        private static TextAlign alignment = TextAlign.Unknown;
        private Document document = null;
        private Page curPage = null;
        private bool selectAll = false;
        private bool pageNumChanging = false;

        public static TextAlign GetAlignment() { return alignment; }
        public static Encoding openFileEncoding = Encoding.UTF8;

        // Helper interfaces
        private void processClosing()
        {
            if( document != null && curPage != null) { document.UpdatePage(curPage); }
            if( document != null && document.isUpdated())
            {
                if( MessageBox.Show("작업 중인 파일을 저장할까요?", "Exit App", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    if (document.targetFilePath == null)
                    {
                        if (saveFileDig.ShowDialog() == DialogResult.OK)
                        {
                            document.targetFilePath = saveFileDig.FileName;
                        }
                        else
                        {
                            if (MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "Exit App", MessageBoxButtons.YesNo) == DialogResult.No)
                                return;
                        }
                    }

                    if (document.targetFilePath != null)
                    {
                        document.SaveTo();
                    }
                }
                else
                {
                    if (MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "Exit App", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                }
            }
        }
        
        private void refresh()
        {
            panelPage.Refresh();
        }


        private void represent()
        {
            if( document != null)
            {
                toolStripLabelTotPages.Text = "/" + Convert.ToString(document.GetPageCount());
            }
            if( curPage != null)
            {
                textBoxPage.Text = "";
                alignmentHandler( curPage.align);

                foreach( string aLine in curPage.textLines)
                {
                    if (textBoxPage.Text != "") textBoxPage.Text += "\r\n";
                    textBoxPage.Text += aLine;
                }
            }
        }

        private void lineSelectHandler(int lines)
        {
            switch (lines)
            {
                case 1:
                    oneLinePageToolStripMenuItem.Checked = true;
                    twoLinePageToolStripMenuItem.Checked = false;

                    toolStripButton1LinePerPage.Checked = true;
                    toolStripButton2LinePerPage.Checked = false;

                    break;
                case 2:
                    oneLinePageToolStripMenuItem.Checked = false;
                    twoLinePageToolStripMenuItem.Checked = true;

                    toolStripButton1LinePerPage.Checked = false;
                    toolStripButton2LinePerPage.Checked = true;

                    break;
                default:
                    oneLinePageToolStripMenuItem.Checked = false;
                    twoLinePageToolStripMenuItem.Checked = false;

                    toolStripButton1LinePerPage.Checked = false;
                    toolStripButton2LinePerPage.Checked = false;
                    break;
            }

            if( lines > 0 && lines <= 8)
            {
                if( multiLinePageStripComboBoxMenu.SelectedIndex != lines - 1) multiLinePageStripComboBoxMenu.SelectedIndex = lines - 1;
                if( toolStripMultilinePerPageComboBox.SelectedIndex != lines - 1) toolStripMultilinePerPageComboBox.SelectedIndex = lines - 1;
            }
            else if( lines > 8)
            {
                return;
            }

            if (Document.linesPerPage == lines) return;

            int pageNo = -1;
            if (document != null && curPage != null)
            {
                if( document.UpdatePage(curPage))
                {
                    pageNo = (curPage.GetPageNo() * Document.linesPerPage) / lines + 1;
                    if( pageNo * lines >= document.GetLineCount() )
                    {
                        pageNo = document.GetLineCount() / lines;
                        if (document.GetLineCount() % lines != 0) pageNo++;

                    }
                }
            }

            Document.linesPerPage = lines;

            if( document != null)
            {
                document.UpdateAll();
                if( pageNo != -1)
                {
                    curPage = document.GetPage(pageNo);

                    represent();
                    refresh();

                    toolStripTextBoxCurPage.Text = Convert.ToString(pageNo);
                }
            }

            selectAllHandler(false);
        }

        private void alignmentHandler( TextAlign a)
        {
            if (alignment == a) return;

            leftToolStripMenuItem.Checked = false;
            toolStripButtonLeftAlign.Checked = false;

            centerToolStripMenuItem.Checked = false;
            toolStripButtonCenterAlign.Checked = false;

            rightToolStripMenuItem.Checked = false;
            toolStripButtonRightAlign.Checked = false;

            switch ( a)
            {
                case TextAlign.Left:
                    leftToolStripMenuItem.Checked = true;
                    toolStripButtonLeftAlign.Checked = true;
                    break;

                case TextAlign.Center:
                    centerToolStripMenuItem.Checked = true;
                    toolStripButtonCenterAlign.Checked = true;

                    break;

                case TextAlign.Right:
                    rightToolStripMenuItem.Checked = true;
                    toolStripButtonRightAlign.Checked = true;
                    break;

                case TextAlign.Unknown:
                    break;
            }

            alignment = a;

            // TODO : rearrange alignment
            if ( curPage != null)
            {
                curPage.align = a;
            }

            if( selectAll && document != null)
            {
                document.ChageAlignAll(a);
            }
            selectAllHandler(false);

            refresh();
        }

        private void selectAllHandler( bool selectedAll)
        {
            selectAll = selectedAll;

            selectAllToolStripMenuItem.Enabled = !selectAll;
            toolStripButtonSelectAllPages.Enabled = !selectAll;
        }

        private void pageMove( int page)
        {
            if ( document != null)
            {
                if (curPage != null)
                {
                    if (curPage.GetPageNo() == page) return;

                    document.UpdatePage(curPage);
                }

                if( page > document.GetPageCount())
                {
                    if (MessageBox.Show("Add a New Page? - 새로운 페이지를 추가하시겠습니까?", "CaptionEditor", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                }

                Page target = document.GetPage(page);

                if( target != null)
                {
                    toolStripTextBoxCurPage.Text = Convert.ToString(target.GetPageNo());
                    curPage = target;

                    represent();
                    refresh();
                }
            }
        }

        private void addNewPage()
        {
            if( document != null) {
                if( curPage != null)
                {
                    document.UpdatePage(curPage);
                }

                if( MessageBox.Show("Add a New Page? - 새로운 페이지를 추가하시겠습니까?", "CaptionEditor", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;

                Page newPage = document.InsertPage(curPage.GetPageNo());
                if( newPage != null)
                {
                    toolStripTextBoxCurPage.Text = Convert.ToString(newPage.GetPageNo());
                    curPage = newPage;

                    represent();
                    refresh();
                }
            }
        }

        // View Codes

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lineSelectHandler(2);
            alignmentHandler(TextAlign.Unknown);
            toolStripSplitEncoding.Text = openFileEncoding == Encoding.UTF8 ? "UTF8" : "ASCII";
            represent();
            refresh();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (document != null && curPage != null) { document.UpdatePage(curPage); }
            if (document != null && document.isUpdated())
            {
                if (document.targetFilePath == null)
                {
                    if (saveFileDig.ShowDialog() == DialogResult.OK)
                    {
                        document.targetFilePath = saveFileDig.FileName;
                    }
                }

                if (document.targetFilePath != null)
                {
                    document.SaveTo();
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ( document != null && curPage != null) { document.UpdatePage(curPage); }
            if ( document != null && document.isUpdated())
            {
                if( MessageBox.Show("작업 중인 파일을 저장할까요?", "New File", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    if (document.targetFilePath == null)
                    {
                        if (saveFileDig.ShowDialog() == DialogResult.OK)
                        {
                            document.targetFilePath = saveFileDig.FileName;
                        }
                        else
                        {
                            if( MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "New File", MessageBoxButtons.YesNo) == DialogResult.No) 
                                return;
                        }
                    }

                    if( document.targetFilePath != null) {
                        document.SaveTo();
                    }
                }
                else
                {
                    if (MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "New File", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                }
            }
            document = new Document();
            curPage = document.AddPage();
            
            represent();
            refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                if (document != null && curPage != null) { document.UpdatePage(curPage); }
                if (document != null && document.isUpdated())
                {
                    
                    if (MessageBox.Show("작업 중인 파일을 저장할까요?", "Open File", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        if (document.targetFilePath == null)
                        {
                            if (saveFileDig.ShowDialog() == DialogResult.OK)
                            {
                                document.targetFilePath = saveFileDig.FileName;
                            }
                            else
                            {
                                if (MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "Open File", MessageBoxButtons.YesNo) == DialogResult.No)
                                    return;
                            }
                        }

                        if (document.targetFilePath != null)
                        {
                            document.SaveTo();
                        }
                    }
                    else
                    {
                        if (MessageBox.Show("작업 중인 파일 내용이 사라집니다. 괜찮습니까?", "Open File", MessageBoxButtons.YesNo) == DialogResult.No)
                            return;
                    }
                }

                document = Document.Open(openFileDlg.FileName);
                curPage = null;

                if( document != null)
                {
                    curPage = document.GetPage(1);
                    lineSelectHandler(multiLinePageStripComboBoxMenu.SelectedIndex + 1);
                }
                represent();
                refresh();
            }
        }

        private void genCGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDig.ShowDialog() == DialogResult.OK)
            {
                if (document != null)
                {
                    if (curPage != null)
                    {
                        document.UpdatePage(curPage);
                    }

                    document.ExportTo( saveFileDig.FileName);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void oneLinePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("1 / 1 page clicked!");
            if( oneLinePageToolStripMenuItem.Checked == true)
            {
                lineSelectHandler(1);
            }
            else
            {
                lineSelectHandler(2);
            }
        }

        private void twoLinePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if( twoLinePageToolStripMenuItem.Checked == true)
            {
                lineSelectHandler(2);
            }
            else
            {
                lineSelectHandler(1);
            }
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if( leftToolStripMenuItem.Checked == true)
            {
                alignmentHandler(TextAlign.Left);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (centerToolStripMenuItem.Checked == true)
            {
                alignmentHandler(TextAlign.Center);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rightToolStripMenuItem.Checked == true)
            {
                alignmentHandler(TextAlign.Right);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectAllHandler(selectAllToolStripMenuItem.Enabled);
        }

        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addNewPage();
        }


        private void toolStripButton1LinePerPage_Click(object sender, EventArgs e)
        {
            if (toolStripButton1LinePerPage.Checked == true)
            {
                lineSelectHandler(1);
            }
            else
            {
                lineSelectHandler(2);
            }
        }

        private void toolStripButton2LinePerPage_Click(object sender, EventArgs e)
        {
            if( toolStripButton2LinePerPage.Checked == true)
            {
                lineSelectHandler(2);
            } else
            {
                lineSelectHandler(1);
            }
        }

        private void toolStripButtonLeftAlign_Click(object sender, EventArgs e)
        {
            if (toolStripButtonLeftAlign.Checked == true)
            {
                alignmentHandler(TextAlign.Left);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void toolStripButtonCenterAlign_Click(object sender, EventArgs e)
        {
            if (toolStripButtonCenterAlign.Checked == true)
            {
                alignmentHandler(TextAlign.Center);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void toolStripButtonRightAlign_Click(object sender, EventArgs e)
        {
            if (toolStripButtonRightAlign.Checked == true)
            {
                alignmentHandler(TextAlign.Right);
            }
            else
            {
                alignmentHandler(TextAlign.Unknown);
            }
        }

        private void toolStripButtonSelectAllPages_Click(object sender, EventArgs e)
        {
            selectAllHandler(toolStripButtonSelectAllPages.Enabled);
        }

        private void toolStripButtonNewPage_Click(object sender, EventArgs e)
        {
            addNewPage();
        }

        private void toolStripButtonNextPage_Click(object sender, EventArgs e)
        {
            int curPage = Convert.ToInt32(toolStripTextBoxCurPage.Text);
            pageMove(curPage + 1);
        }

        private void toolStripButtonPrevPage_Click(object sender, EventArgs e)
        {
            int curPage = Convert.ToInt32(toolStripTextBoxCurPage.Text);
            pageMove(curPage - 1);
        }

        private void toolStripTextBoxCurPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && pageNumChanging == true)
            {
                pageNumChanging = false;
                pageMove(Convert.ToInt32(toolStripTextBoxCurPage.Text));
            }
            else
            {
                pageNumChanging = true;
            }
        }

        private void toolStripTextBoxCurPage_Leave(object sender, EventArgs e)
        {
            if (pageNumChanging == true)
            {
                pageNumChanging = false;
                pageMove(Convert.ToInt32(toolStripTextBoxCurPage.Text));
            }
        }

        private void textBoxPage_TextChanged(object sender, EventArgs e)
        {
            refresh();
        }

        private void textBoxPage_KeyUp(object sender, KeyEventArgs e)
        {
            if (document == null)
            {
                document = new Document();
                curPage = document.AddPage();
            }

            if ( curPage != null && document != null)
            {
                curPage.textLines = new List<string>(textBoxPage.Lines);
                document.UpdatePage(curPage);
            }
        }

        private void panelPage_Paint(object sender, PaintEventArgs e)
        {
            using( SolidBrush br = new SolidBrush(Color.White))
            {
                float padx = ((float)panelPage.Size.Width) * (0.1f);
                float pady = ((float)panelPage.Size.Height) * (0.1f);

                float yOffsetFactor = (Document.linesPerPage == 1) ? 7.0f : (9.0f - (float)Document.linesPerPage);
                float width = ((float)panelPage.Size.Width) - 2 * padx;
                float height = ((float)panelPage.Size.Height) - (yOffsetFactor+1) * pady;
                float yOffset = ((float)panelPage.Size.Height) * yOffsetFactor/10.0f;
                
                StringFormat sf = new StringFormat();


                switch( alignment)
                {
                    case TextAlign.Center:
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Alignment = StringAlignment.Center;
                        break;

                    case TextAlign.Left:
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Alignment = StringAlignment.Near;
                        break;

                    case TextAlign.Right:
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Alignment = StringAlignment.Far;
                        break;

                    case TextAlign.Unknown:
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Alignment = StringAlignment.Center;
                        break;

                }

                e.Graphics.DrawString(textBoxPage.Text, panelPage.Font, br, new RectangleF( padx, yOffset, width, height), sf);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            processClosing();
        }

        private void toolStripSplitEncoding_ButtonClick(object sender, EventArgs e)
        {
            if( openFileEncoding == Encoding.UTF8)
            {
                openFileEncoding = Encoding.Default;
            }
            else
            {
                openFileEncoding = Encoding.UTF8;
            }
            toolStripSplitEncoding.Text = toolStripSplitEncoding.Text = openFileEncoding == Encoding.UTF8 ? "UTF8" : "ASCII"; ;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "\tPresented by goovro.com. v1.0.1   \t\n\n\tⓒ2019 All Rights Reserved.    \t";
            MessageBox.Show( message, "About", MessageBoxButtons.OK);
        }

        private void multiLinePageStripComboBoxMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            lineSelectHandler(multiLinePageStripComboBoxMenu.SelectedIndex + 1);

        }

        private void toolStripMultilinePerPageComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            lineSelectHandler(toolStripMultilinePerPageComboBox.SelectedIndex + 1);
        }
    }
}
