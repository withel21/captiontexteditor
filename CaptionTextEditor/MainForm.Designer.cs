﻿namespace CaptionTextEditor
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genCGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oneLinePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twoLinePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiLinePageStripComboBoxMenu = new System.Windows.Forms.ToolStripComboBox();
            this.alignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.addNewPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.panelPageList = new System.Windows.Forms.Panel();
            this.panelPageControl = new System.Windows.Forms.Panel();
            this.textBoxPage = new System.Windows.Forms.TextBox();
            this.panelPage = new System.Windows.Forms.Panel();
            this.toolMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonLeftAlign = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCenterAlign = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRightAlign = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSelectAllPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1LinePerPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2LinePerPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripMultilinePerPageComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonPrevPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxCurPage = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabelTotPages = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonNextPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNewPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitEncoding = new System.Windows.Forms.ToolStripSplitButton();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDig = new System.Windows.Forms.SaveFileDialog();
            this.menuMain.SuspendLayout();
            this.panelPageControl.SuspendLayout();
            this.toolMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.genCGToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // genCGToolStripMenuItem
            // 
            this.genCGToolStripMenuItem.Name = "genCGToolStripMenuItem";
            this.genCGToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.G)));
            this.genCGToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.genCGToolStripMenuItem.Text = "GenCG Text";
            this.genCGToolStripMenuItem.Click += new System.EventHandler(this.genCGToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignmentToolStripMenuItem,
            this.alignmentToolStripMenuItem,
            this.toolStripSeparator3,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator8,
            this.addNewPageToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // assignmentToolStripMenuItem
            // 
            this.assignmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oneLinePageToolStripMenuItem,
            this.twoLinePageToolStripMenuItem,
            this.multiLinePageStripComboBoxMenu});
            this.assignmentToolStripMenuItem.Name = "assignmentToolStripMenuItem";
            this.assignmentToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.assignmentToolStripMenuItem.Text = "Assignment";
            // 
            // oneLinePageToolStripMenuItem
            // 
            this.oneLinePageToolStripMenuItem.CheckOnClick = true;
            this.oneLinePageToolStripMenuItem.Name = "oneLinePageToolStripMenuItem";
            this.oneLinePageToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.oneLinePageToolStripMenuItem.Text = "1 / 1 page";
            this.oneLinePageToolStripMenuItem.Click += new System.EventHandler(this.oneLinePageToolStripMenuItem_Click);
            // 
            // twoLinePageToolStripMenuItem
            // 
            this.twoLinePageToolStripMenuItem.Checked = true;
            this.twoLinePageToolStripMenuItem.CheckOnClick = true;
            this.twoLinePageToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.twoLinePageToolStripMenuItem.Name = "twoLinePageToolStripMenuItem";
            this.twoLinePageToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.twoLinePageToolStripMenuItem.Text = "2 / 1 page";
            this.twoLinePageToolStripMenuItem.Click += new System.EventHandler(this.twoLinePageToolStripMenuItem_Click);
            // 
            // multiLinePageStripComboBoxMenu
            // 
            this.multiLinePageStripComboBoxMenu.AutoSize = false;
            this.multiLinePageStripComboBoxMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.multiLinePageStripComboBoxMenu.Items.AddRange(new object[] {
            "1 / 1 page",
            "2 / 1 page",
            "3 / 1 page",
            "4 / 1 page",
            "5 / 1 page",
            "6 / 1 page",
            "7 / 1 page",
            "8 / 1 page"});
            this.multiLinePageStripComboBoxMenu.Name = "multiLinePageStripComboBoxMenu";
            this.multiLinePageStripComboBoxMenu.Size = new System.Drawing.Size(121, 23);
            this.multiLinePageStripComboBoxMenu.SelectedIndexChanged += new System.EventHandler(this.multiLinePageStripComboBoxMenu_SelectedIndexChanged);
            // 
            // alignmentToolStripMenuItem
            // 
            this.alignmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftToolStripMenuItem,
            this.centerToolStripMenuItem,
            this.rightToolStripMenuItem});
            this.alignmentToolStripMenuItem.Name = "alignmentToolStripMenuItem";
            this.alignmentToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.alignmentToolStripMenuItem.Text = "Alignment";
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.CheckOnClick = true;
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Left)));
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.leftToolStripMenuItem.Text = "Left";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.leftToolStripMenuItem_Click);
            // 
            // centerToolStripMenuItem
            // 
            this.centerToolStripMenuItem.CheckOnClick = true;
            this.centerToolStripMenuItem.Name = "centerToolStripMenuItem";
            this.centerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Down)));
            this.centerToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.centerToolStripMenuItem.Text = "Center";
            this.centerToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.CheckOnClick = true;
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Right)));
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.rightToolStripMenuItem.Text = "Right";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.rightToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(221, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.selectAllToolStripMenuItem.Text = "Select All Pages";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(221, 6);
            // 
            // addNewPageToolStripMenuItem
            // 
            this.addNewPageToolStripMenuItem.Name = "addNewPageToolStripMenuItem";
            this.addNewPageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.N)));
            this.addNewPageToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.addNewPageToolStripMenuItem.Text = "Add New Page";
            this.addNewPageToolStripMenuItem.Click += new System.EventHandler(this.addNewPageToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(1206, 24);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(150, 150);
            // 
            // panelPageList
            // 
            this.panelPageList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPageList.Location = new System.Drawing.Point(0, 28);
            this.panelPageList.Name = "panelPageList";
            this.panelPageList.Size = new System.Drawing.Size(222, 655);
            this.panelPageList.TabIndex = 1;
            // 
            // panelPageControl
            // 
            this.panelPageControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPageControl.Controls.Add(this.textBoxPage);
            this.panelPageControl.Controls.Add(this.panelPage);
            this.panelPageControl.Controls.Add(this.toolMain);
            this.panelPageControl.Location = new System.Drawing.Point(223, 27);
            this.panelPageControl.Name = "panelPageControl";
            this.panelPageControl.Size = new System.Drawing.Size(982, 656);
            this.panelPageControl.TabIndex = 2;
            // 
            // textBoxPage
            // 
            this.textBoxPage.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPage.Font = new System.Drawing.Font("굴림체", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPage.Location = new System.Drawing.Point(9, 578);
            this.textBoxPage.Multiline = true;
            this.textBoxPage.Name = "textBoxPage";
            this.textBoxPage.Size = new System.Drawing.Size(960, 72);
            this.textBoxPage.TabIndex = 2;
            this.textBoxPage.TextChanged += new System.EventHandler(this.textBoxPage_TextChanged);
            this.textBoxPage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxPage_KeyUp);
            // 
            // panelPage
            // 
            this.panelPage.BackColor = System.Drawing.SystemColors.ControlText;
            this.panelPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPage.Font = new System.Drawing.Font("굴림체", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panelPage.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPage.Location = new System.Drawing.Point(9, 32);
            this.panelPage.Name = "panelPage";
            this.panelPage.Size = new System.Drawing.Size(960, 540);
            this.panelPage.TabIndex = 1;
            this.panelPage.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPage_Paint);
            // 
            // toolMain
            // 
            this.toolMain.Dock = System.Windows.Forms.DockStyle.None;
            this.toolMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonLeftAlign,
            this.toolStripButtonCenterAlign,
            this.toolStripButtonRightAlign,
            this.toolStripSeparator4,
            this.toolStripButtonSelectAllPages,
            this.toolStripSeparator5,
            this.toolStripButton1LinePerPage,
            this.toolStripButton2LinePerPage,
            this.toolStripMultilinePerPageComboBox,
            this.toolStripSeparator6,
            this.toolStripButtonPrevPage,
            this.toolStripTextBoxCurPage,
            this.toolStripLabelTotPages,
            this.toolStripButtonNextPage,
            this.toolStripSeparator7,
            this.toolStripButtonNewPage,
            this.toolStripSeparator9,
            this.toolStripSplitEncoding});
            this.toolMain.Location = new System.Drawing.Point(0, 0);
            this.toolMain.Name = "toolMain";
            this.toolMain.Padding = new System.Windows.Forms.Padding(0);
            this.toolMain.Size = new System.Drawing.Size(473, 31);
            this.toolMain.TabIndex = 0;
            this.toolMain.Text = "toolbar";
            // 
            // toolStripButtonLeftAlign
            // 
            this.toolStripButtonLeftAlign.CheckOnClick = true;
            this.toolStripButtonLeftAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLeftAlign.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLeftAlign.Image")));
            this.toolStripButtonLeftAlign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLeftAlign.Name = "toolStripButtonLeftAlign";
            this.toolStripButtonLeftAlign.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonLeftAlign.Text = "Left Align";
            this.toolStripButtonLeftAlign.ToolTipText = "Left Align";
            this.toolStripButtonLeftAlign.Click += new System.EventHandler(this.toolStripButtonLeftAlign_Click);
            // 
            // toolStripButtonCenterAlign
            // 
            this.toolStripButtonCenterAlign.CheckOnClick = true;
            this.toolStripButtonCenterAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCenterAlign.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCenterAlign.Image")));
            this.toolStripButtonCenterAlign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCenterAlign.Name = "toolStripButtonCenterAlign";
            this.toolStripButtonCenterAlign.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonCenterAlign.Text = "Center Align";
            this.toolStripButtonCenterAlign.ToolTipText = "Center Align";
            this.toolStripButtonCenterAlign.Click += new System.EventHandler(this.toolStripButtonCenterAlign_Click);
            // 
            // toolStripButtonRightAlign
            // 
            this.toolStripButtonRightAlign.CheckOnClick = true;
            this.toolStripButtonRightAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRightAlign.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRightAlign.Image")));
            this.toolStripButtonRightAlign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRightAlign.Name = "toolStripButtonRightAlign";
            this.toolStripButtonRightAlign.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonRightAlign.Text = "Right Align";
            this.toolStripButtonRightAlign.ToolTipText = "Right Align";
            this.toolStripButtonRightAlign.Click += new System.EventHandler(this.toolStripButtonRightAlign_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonSelectAllPages
            // 
            this.toolStripButtonSelectAllPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSelectAllPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSelectAllPages.Image")));
            this.toolStripButtonSelectAllPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSelectAllPages.Name = "toolStripButtonSelectAllPages";
            this.toolStripButtonSelectAllPages.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonSelectAllPages.Text = "Select All Pages";
            this.toolStripButtonSelectAllPages.ToolTipText = "Select All Pages";
            this.toolStripButtonSelectAllPages.Click += new System.EventHandler(this.toolStripButtonSelectAllPages_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButton1LinePerPage
            // 
            this.toolStripButton1LinePerPage.CheckOnClick = true;
            this.toolStripButton1LinePerPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1LinePerPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1LinePerPage.Image")));
            this.toolStripButton1LinePerPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1LinePerPage.Name = "toolStripButton1LinePerPage";
            this.toolStripButton1LinePerPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton1LinePerPage.Text = "1 Line per Page";
            this.toolStripButton1LinePerPage.ToolTipText = "1 Line per Page";
            this.toolStripButton1LinePerPage.Click += new System.EventHandler(this.toolStripButton1LinePerPage_Click);
            // 
            // toolStripButton2LinePerPage
            // 
            this.toolStripButton2LinePerPage.CheckOnClick = true;
            this.toolStripButton2LinePerPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2LinePerPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2LinePerPage.Image")));
            this.toolStripButton2LinePerPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2LinePerPage.Name = "toolStripButton2LinePerPage";
            this.toolStripButton2LinePerPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton2LinePerPage.Text = "2 Lines per Page";
            this.toolStripButton2LinePerPage.ToolTipText = "2 Lines per Page";
            this.toolStripButton2LinePerPage.Click += new System.EventHandler(this.toolStripButton2LinePerPage_Click);
            // 
            // toolStripMultilinePerPageComboBox
            // 
            this.toolStripMultilinePerPageComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripMultilinePerPageComboBox.Items.AddRange(new object[] {
            "1 / 1 page",
            "2 / 1 page",
            "3 / 1 page",
            "4 / 1 page",
            "5 / 1 page",
            "6 / 1 page",
            "7 / 1 page",
            "8 / 1 page"});
            this.toolStripMultilinePerPageComboBox.Name = "toolStripMultilinePerPageComboBox";
            this.toolStripMultilinePerPageComboBox.Size = new System.Drawing.Size(121, 31);
            this.toolStripMultilinePerPageComboBox.SelectedIndexChanged += new System.EventHandler(this.toolStripMultilinePerPageComboBox_SelectedIndexChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonPrevPage
            // 
            this.toolStripButtonPrevPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrevPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrevPage.Image")));
            this.toolStripButtonPrevPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrevPage.Name = "toolStripButtonPrevPage";
            this.toolStripButtonPrevPage.Size = new System.Drawing.Size(23, 28);
            this.toolStripButtonPrevPage.Text = "◀";
            this.toolStripButtonPrevPage.ToolTipText = "Previous page";
            this.toolStripButtonPrevPage.Click += new System.EventHandler(this.toolStripButtonPrevPage_Click);
            // 
            // toolStripTextBoxCurPage
            // 
            this.toolStripTextBoxCurPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxCurPage.Name = "toolStripTextBoxCurPage";
            this.toolStripTextBoxCurPage.Size = new System.Drawing.Size(30, 31);
            this.toolStripTextBoxCurPage.Text = "1";
            this.toolStripTextBoxCurPage.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolStripTextBoxCurPage.Leave += new System.EventHandler(this.toolStripTextBoxCurPage_Leave);
            this.toolStripTextBoxCurPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBoxCurPage_KeyDown);
            // 
            // toolStripLabelTotPages
            // 
            this.toolStripLabelTotPages.Name = "toolStripLabelTotPages";
            this.toolStripLabelTotPages.Size = new System.Drawing.Size(19, 28);
            this.toolStripLabelTotPages.Text = "/1";
            // 
            // toolStripButtonNextPage
            // 
            this.toolStripButtonNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonNextPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNextPage.Image")));
            this.toolStripButtonNextPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNextPage.Name = "toolStripButtonNextPage";
            this.toolStripButtonNextPage.Size = new System.Drawing.Size(23, 28);
            this.toolStripButtonNextPage.Text = "▶";
            this.toolStripButtonNextPage.ToolTipText = "Next page";
            this.toolStripButtonNextPage.Click += new System.EventHandler(this.toolStripButtonNextPage_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonNewPage
            // 
            this.toolStripButtonNewPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNewPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNewPage.Image")));
            this.toolStripButtonNewPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNewPage.Name = "toolStripButtonNewPage";
            this.toolStripButtonNewPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonNewPage.Text = "New Page";
            this.toolStripButtonNewPage.ToolTipText = "new page";
            this.toolStripButtonNewPage.Click += new System.EventHandler(this.toolStripButtonNewPage_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripSplitEncoding
            // 
            this.toolStripSplitEncoding.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitEncoding.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitEncoding.Image")));
            this.toolStripSplitEncoding.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitEncoding.Name = "toolStripSplitEncoding";
            this.toolStripSplitEncoding.Size = new System.Drawing.Size(16, 28);
            this.toolStripSplitEncoding.ToolTipText = "open file encoding";
            this.toolStripSplitEncoding.ButtonClick += new System.EventHandler(this.toolStripSplitEncoding_ButtonClick);
            // 
            // openFileDlg
            // 
            this.openFileDlg.DefaultExt = "txt";
            // 
            // saveFileDig
            // 
            this.saveFileDig.DefaultExt = "txt";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1206, 684);
            this.Controls.Add(this.panelPageControl);
            this.Controls.Add(this.panelPageList);
            this.Controls.Add(this.menuMain);
            this.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuMain;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "CaptionTextEditor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.panelPageControl.ResumeLayout(false);
            this.panelPageControl.PerformLayout();
            this.toolMain.ResumeLayout(false);
            this.toolMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genCGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oneLinePageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twoLinePageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.Panel panelPageList;
        private System.Windows.Forms.Panel panelPageControl;
        private System.Windows.Forms.ToolStrip toolMain;
        private System.Windows.Forms.ToolStripButton toolStripButtonLeftAlign;
        private System.Windows.Forms.ToolStripButton toolStripButtonCenterAlign;
        private System.Windows.Forms.ToolStripButton toolStripButtonRightAlign;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonSelectAllPages;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton1LinePerPage;
        private System.Windows.Forms.ToolStripButton toolStripButton2LinePerPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.Panel panelPage;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.SaveFileDialog saveFileDig;
        private System.Windows.Forms.TextBox textBoxPage;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrevPage;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxCurPage;
        private System.Windows.Forms.ToolStripLabel toolStripLabelTotPages;
        private System.Windows.Forms.ToolStripButton toolStripButtonNextPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem addNewPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonNewPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitEncoding;
        private System.Windows.Forms.ToolStripComboBox multiLinePageStripComboBoxMenu;
        private System.Windows.Forms.ToolStripComboBox toolStripMultilinePerPageComboBox;
    }
}

