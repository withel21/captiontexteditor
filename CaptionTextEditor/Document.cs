﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaptionTextEditor
{
    public enum DocumentType { Plain, GenCGTxt };
    public class Document
    {
        private static string uas = "↕";
        private static string las = "←";
        private static string cas = "↔";
        private static string ras = "→";
        private static string LINESPERPAGESTR = "LINESPERPAGE=";
        private List<string> lines;
        public string targetFilePath = null;
        private bool isDirty = false;

        static public int linesPerPage = 2;

        public Document()
        {
            lines = new List<string>();
        }

        public bool isUpdated() { return isDirty; }

        static public Document Open(string filePath) {
            Document newDoc = new Document();

            using (System.IO.StreamReader r = new System.IO.StreamReader(filePath, MainForm.openFileEncoding))
            {
                string curLine, prefix = null;

                while ((curLine = r.ReadLine()) != null)
                {
                    if (newDoc.lines.Count() == 0 && curLine.StartsWith(LINESPERPAGESTR))
                    {
                        linesPerPage = Convert.ToInt32(curLine.Substring(LINESPERPAGESTR.Length));
                        continue;
                    }
                    else
                    {
                        // 이전 라인 정렬 설정 승계
                        string curPrefix = GetAlignPrefix(GetAlignFromLineText(curLine));;
                        if (IsStartFromAlignId(curLine) == false)
                        {
                            if (prefix == null) prefix = uas;
                            curLine = prefix + curLine;
                        }
                        else
                        {
                            prefix = curPrefix;
                        }
                    }
                    newDoc.lines.Add(curLine);
                }
                r.Close();
            }

            newDoc.targetFilePath = filePath;

            return newDoc;
        }

        public bool IsEmpty()
        {
            return (lines == null || lines.Count() == 0);
        }

        public int GetPageCount()
        {
            return (lines.Count() / linesPerPage) + ((lines.Count() % linesPerPage == 0) ? 0 : 1);
        }


        public bool SaveTo(string filePath = null)
        {
            if (filePath == null)
            {
                filePath = targetFilePath;
            }

            if (filePath != null)
            {
                using (System.IO.StreamWriter w = System.IO.File.CreateText(filePath))
                {

                    w.WriteLine(LINESPERPAGESTR + Convert.ToString(linesPerPage));

                    foreach (string aLine in lines)
                    {
                        w.WriteLine(aLine);
                    }
                }
                isDirty = false;
                return true;
            }
            return false;
        }

        public bool ExportTo(string filePath, DocumentType type = DocumentType.GenCGTxt)
        {
            switch( type)
            {
                case DocumentType.GenCGTxt:
                    return ExportGenCGText(filePath);

                case DocumentType.Plain:
                    return SaveTo(filePath);
            }

            return false;
        }

        public int GetLineCount()
        {
            if (lines != null)
            {
                return lines.Count();
            }
            return 0;
        }

        public Page AddPage()
        {
            TextAlign align = (lines.Count() != 0)?GetAlignFromLineText(lines[lines.Count()-1]):TextAlign.Unknown;

            while(lines.Count() != 0 && linesPerPage != 1 && lines.Count() % linesPerPage != 0) {
                lines.Add(GetAlignPrefix(align));
            }
            align = MainForm.GetAlignment();

            List<string> pageString = new List<String>();
            for( int i = 0; i < linesPerPage; i++)
            {
                string prefix = GetAlignPrefix(align);
                lines.Add(prefix);
                pageString.Add(prefix);
            }
            isDirty = true;
            return new Page(pageString, lines.Count()/linesPerPage);
        }

        public Page InsertPage( int page)
        {
            TextAlign align = MainForm.GetAlignment();
            int lineIdx = (page - 1) * linesPerPage;

            List<string> pageString = new List<String>();
            for (int i = 0; i < linesPerPage; i++)
            {
                string prefix = GetAlignPrefix(align);
                lines.Insert(lineIdx + i, prefix);
                pageString.Add(prefix);
            }
            isDirty = true;
            return new Page(pageString, lines.Count() / linesPerPage);
        }

        public Page GetPage( int page)
        {
            if (page <= 0) return null;
            int lineIdx = (page - 1) * linesPerPage;

            if (lineIdx >= lines.Count()) {
                return AddPage();
            }
            while( lines.Count() < page*linesPerPage)
            {
                string prefix = GetAlignPrefix(MainForm.GetAlignment());
                lines.Add(prefix);
                isDirty = true;
            }

            List<string> pageString = lines.GetRange(lineIdx, linesPerPage);

            return new Page( pageString, page);
        }

        public bool DeletePage(Page page)
        {
            int lineIdx = (page.GetPageNo() - 1) * linesPerPage;

            if (lineIdx >= lines.Count()) return false;

            lines.RemoveRange(lineIdx, linesPerPage);
            isDirty = true;

            return true;
        }

        public bool UpdatePage(Page page)
        {
            int lineIdx = (page.GetPageNo() - 1) * linesPerPage;
            if (lineIdx >= lines.Count()) return false;
            List<string> updates = page.GetPageForDocument();
            bool updated = false;
            for( int i = 0; i < updates.Count(); i++)
            {
                if( i >= linesPerPage ||
                    lineIdx + i >= lines.Count())
                {
                    //string message = "\tline count is over....\t";
                    //MessageBox.Show(message, "ERROR", MessageBoxButtons.OK);
                    return false;
                }
                if (lines[lineIdx + i] != updates[i])
                {
                    updated = true;
                    lines[lineIdx + i] = updates[i];
                }
            }
            if( updated) isDirty = true;
            return true;
        }

        public bool ChageAlignAll(TextAlign align)
        {
            string prefix = GetAlignPrefix(align);
            for (int i = 0; i < lines.Count(); i++)
            {
                lines[i] = prefix + lines[i].Remove(0, 1);
            }
            isDirty = true;
            return true;
        }

        public void UpdateAll()
        {
            if( linesPerPage > 1)
            {
                for( int i = 0; i < lines.Count(); i+=linesPerPage)
                {
                    string prefix = lines[i].Length > 0 ? lines[i].Substring(0, 1) : uas;

                    for (int j = 1; j < linesPerPage; j++)
                    {
                        if (i + j >= lines.Count())
                        {
                            lines.Add(prefix);
                        }
                        else
                        {
                            lines[i + j] = prefix + lines[i + j].Remove(0, 1);
                        }
                    }
                }
            }
            isDirty = true;
        }


        private bool ExportGenCGText(string filePath)
        {
            int euckrCodepage = 51949;
            bool ret = false;
            System.Text.Encoding euckr = System.Text.Encoding.GetEncoding(euckrCodepage);

            if (filePath != null)
            {
                TextAlign align = TextAlign.Unknown;
                System.IO.FileStream fs = null;
                string newPageStr = "//\r\n";
                string lAlignStr = "<<\r\n";
                string cAlignStr = "<>\r\n";
                string rAlignStr = ">>\r\n";
                string uAlignStr = "&&\r\n"; // 영화 자막???

                byte[] newPageData = euckr.GetBytes(newPageStr);
                byte[] lAlignData = euckr.GetBytes(lAlignStr);
                byte[] cAlignData = euckr.GetBytes(cAlignStr);
                byte[] rAlignData = euckr.GetBytes(rAlignStr);
                byte[] uAlignData = euckr.GetBytes(uAlignStr);

                try
                {
                    fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create);

                    using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, euckr))
                    {
                        for( int i = 0; i < lines.Count()/linesPerPage; i++)
                        {
                            for (int j = 0; j < linesPerPage; j++)
                            {
                                int idx = i * linesPerPage + j;

                                string curLine = lines[idx];
                                bool firstCharSkip = IsStartFromAlignId(curLine);
                                TextAlign curAlign = firstCharSkip ? GetAlignFromLineText(curLine) : align;

                                if( j == 0 && curAlign != align) {
                                    align = curAlign;
                                    switch( align) {
                                        case TextAlign.Unknown: w.Write(uAlignStr);    break;
                                        case TextAlign.Left:    w.Write(lAlignStr);    break;
                                        case TextAlign.Center:  w.Write(cAlignStr);    break;
                                        case TextAlign.Right:   w.Write(rAlignStr);    break;

                                    }
                                }

                                string aLine = (firstCharSkip ? curLine.Substring(1) : curLine) + "\r\n";
                                byte[] lineBytes = euckr.GetBytes( aLine);

                                w.Write(aLine);
                            }
                            w.Write(newPageStr);
                        }
                    }
                    ret = true;
                }
                finally
                {
                    if( fs != null)
                    {
                        fs.Dispose();
                    }
                }
            }
            return ret;
        }

        public static string GetAlignPrefix(TextAlign align)
        {
            switch (align)
            {
                case TextAlign.Unknown:
                    return uas;

                case TextAlign.Center:
                    return cas;

                case TextAlign.Left:
                    return las;

                case TextAlign.Right:
                    return ras;
            }
            return uas;
        }

        public static TextAlign GetAlignFromLineText( string lineText)
        {
            string prefix = lineText.Length > 0 ? lineText.Substring(0, 1) : "";

            if (prefix == ras) return TextAlign.Right;
            if (prefix == cas) return TextAlign.Center;
            if (prefix == las) return TextAlign.Left;
            return TextAlign.Unknown;
        }

        public static bool IsStartFromAlignId( string lineText)
        {
            string prefix = lineText.Length > 0 ? lineText.Substring(0, 1) : "";

            return (prefix == uas || prefix == las || prefix == cas || prefix == ras);
        }
    }
}
